/* A função strtok recebe uma string[N] e apartir de um delimitador ele lê a string ate encontrar o delimitador na string e
 * retorna a string ate esse ponto e na proxima chamada ele retorna a string do ponto em que tinha parado
 * ate encontrar o delimitador nomavemente
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  char string[500];
  char delimitador[2] = " ";//delimitador[0] = " " e delimitador[1] = \0 da string
                            //sempre que encontrar um spaço a função vai retornar a string ate essa parte
char *palavra; // a função strtok retorna um endereço pra nova string


printf("Informe a string: ");
scanf(" %[^\n]s", string);
palavra = strtok(string, delimitador);

while (palavra != NULL) {//quando palavra == NULL a funçaõ vai ter chegado ao fim da string
  printf("%s\n", palavra);
  palavra = strtok(NULL, delimitador);
}
  return 0;
}
